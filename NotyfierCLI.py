import Notyfier
import sys


def main():  # run the main of Notyfier library
    Notyfier.main(sys.argv)


if __name__ == "__main__":
    main()
